/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.nakamol.usermanagment;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author OS
 */
public class UserService {

    private static ArrayList<User> userList = new ArrayList<>();

    static {                                                                //Mockup
        userList.add(new User("admin", "password"));
    }

    public static boolean addUser(User user) {                              //Create
        userList.add(user);
        return true;
    }

    public static boolean addUser(String userName, String password) {
        userList.add(new User(userName, password));
        return true;
    }

    public static boolean updateUser(int index, User user) {                //Update
        userList.set(index, user);
        return true;
    }

    public static User getUser(int index) {                                 //Read 1 User
        if (index > userList.size() - 1) {
            return null;
        }
        return userList.get(index);
    }

    public static ArrayList<User> getUsers() {                              //Read all User
        return userList;
    }

    public static ArrayList<User> searchUserName(String serchText) {        //Search userName
        ArrayList<User> list = new ArrayList<>();
        for (User user : userList) {
            if (user.getUserName().startsWith(serchText)) {
                list.add(user);
            }
        }
        return list;
    }

    public static boolean delUser(int index) {                              //Delete user
        userList.remove(index);
        return true;
    }

    public static boolean delUser(User user) {                              //Delete user
        userList.remove(user);
        return true;
    }

    public static User Login(String userName, String password) {            //Login
        for (User user : userList) {
            if (user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                return user;
            }
        }
        return null;
    }

    public static void save() {
        FileOutputStream fos = null;
        try {
            File file = new File("UserManagement.dat");
            fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(UserService.getUsers());
            oos.close();
            fos.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } finally {
            try {
                fos.close();
            } catch (IOException ex) {

            }
        }
    }

    public static void load() {
        FileInputStream fis = null;
        try {
            File file = new File("UserManagement.dat");
            fis = new FileInputStream(file);
            ObjectInputStream ois = new ObjectInputStream(fis);
            ArrayList<User> temp = (ArrayList<User>) ois.readObject();
            UserService.addUser(temp);
            ois.close();
            fis.close();
        } catch (FileNotFoundException ex) {

        } catch (IOException ex) {

        } catch (ClassNotFoundException ex) {

        } finally {
            try {
                fis.close();
            } catch (IOException ex) {

            }
        }
    }

    private static boolean adminCheck() {
        boolean check = false;
        for (User user : userList) {
            if (user.getUserName().equals("admin")) {
                check = true;
                return check;
            }
        }
        return check;
    }

    public static boolean addUser(ArrayList<User> user) {
        userList.addAll(user);
        return true;
    }
}
